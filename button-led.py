#button.py   #using pud_up

import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)
#GPIO pin of the component
Lightpin = 4
Buttonpin = 17

GPIO.setup(Lightpin, GPIO.OUT)
GPIO.setup(Buttonpin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.output(Lightpin, False)

try:
    
    while True:
        GPIO.output(Lightpin, not GPIO.input(Buttonpin))
        sleep(.1)
        
finally:
    GPIO.output(Lightpin, False)
    GPIO.cleanup()

